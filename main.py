from collections import defaultdict
import json
import logging

from flask import Flask, request
from flask.json import jsonify

app = Flask(__name__)
data = defaultdict(int)

@app.route('/admin/data', methods=['GET'])
def get_data():
    hdrs = request.headers.get('User-Agent')
    with open('.admin-header') as fin:
        magic_header = '{0}'.format(fin.read().strip())

    if hdrs == magic_header:
        resp = json.dumps({"result": dict(data)})
    else:
        resp = jsonify('Unauthorized')
        resp.status_code = 401
    return resp


@app.route('/api/<license>', methods=['GET'])
def hello_world(license):
    ret = {}
    [del(score) for score in data if score == 0]
    ret['licenseRequest'] = license
    ret['score'] = data[license]
    return jsonify(ret)


@app.route('/api', methods=['POST'])
def increment():
    license = request.form.get('license')
    data[license] = data[license] + 1
    ret = {}                                                                     
    ret['licenseRequest'] = license                                              
    ret['score'] = data[license]                                                 
    return jsonify(ret)                                                          


@app.route('/')
def home():
    return app.send_static_file('index.html')


@app.route('/<filename>')
def show_file(filename):
    return app.send_static_file(filename)


if __name__ == '__main__':
    logging.basicConfig(level=logging.OFF,format='%(asctime)s %(message)s')
    app.run()
